# Vision Project: Facial Expression Recognition Study Using Local Binary Patterns Descriptors

This project explores the application of Local Binary Patterns (LBP) in the field of facial emotion recognition, without the use of Deep Learning techniques.

## Approach

Our methodology focuses on extracting local texture patterns from facial images with LBP with the aim of reaching a comprehensive representation of the faces.

To validate our approach, evaluations were performed on a benchmarked dataset encompassing varying lighting conditions, poses, and facial expressions. Our method showcased promises, all the more so since it is based on non-deep learning models.
The use of LBP presents a strong basis, as its capacity to capture nuanced facial attributes allows the use of these models in real-world scenarios.

## Repository Contents

This repository contains a Jupyter notebook with all the implemented approaches and all the intermediate results as zip files. Some of the cells may take a long time to run, especially if you don't have enough RAM, so it's recommended to use the load intermediate results. The given cells are specified in the notebook.

## Getting Started

1. Use the `requirements.txt` file to create your virtual environment.
2. Unzip the data folder and the intermediate results.
3. Launch the `project.ipynb` notebook.